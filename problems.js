let compose = function (f, g) {
  return function (x) {
    return f(g(x));
  };
};


let problems = ["3COLORING", "3SAT", "SAT", "picross", "INDEPENDENTSET", "CLIQUE"];

$(document).ready(function () {
  for (let i in problems) {
    $('#problem1').append($('<option>', {
      value: problems[i],
      text: problems[i]
    }));
  }


  problem1choose();


});



function getInstance1() {
  return $("#instance1").val();
}



function getInstance2() {
  return $("#instance2").val();
}





function switchProblems() {
  let problem1 = $("#problem1").val();
  let problem2 = $("#problem2").val();

  if (evalwhetherdefined(getProblemNormalizedName(problem2) + "_to_" + getProblemNormalizedName(problem1))) {
    $("#problem1").val(problem2);
    problem1instanceupdate();
    problems2listupdate();

    $("#problem2").val(problem1);

    update();
  }
}

function getProblem1() {
  return getProblemNormalizedName($("#problem1").val());
}


function getProblem2() {
  return getProblemNormalizedName($("#problem2").val());
}


function getProblemNormalizedName(name) {
  name = name.replace(/3/g, "three");
  name = name.toLowerCase();
  return name;
}




function evalwhetherdefined(expression) {
  return eval("typeof " + expression) != "undefined";

}




function problems2listupdate() {
  let name1 = getProblem1();
  $('#problem2').empty();
  for (let i in problems) {
    if (evalwhetherdefined(name1 + "_to_" + getProblemNormalizedName(problems[i]))) {
      $('#problem2').append($('<option>', {
        value: problems[i],
        text: problems[i]
      }));
    }

  }
}



function problem1instanceupdate() {
  let name1 = getProblem1();
  $("#instance1").val(eval(name1 + "_getInstance()"));
}
function problem1choose() {
  let name1 = getProblem1();
  problem1instanceupdate();
  problems2listupdate();
  update();
}




function update() {
  if (evalwhetherdefined(getProblem1() + "_drawInstance"))
    eval(getProblem1() + "_drawInstance('#instance1div', (getInstance1()))");
  else
    $("#instance1div").html("");

  let reductionName = getProblem1() + "_to_" + getProblem2();
  $("#instance2").val(eval(reductionName + "(getInstance1())"));

  if (evalwhetherdefined(getProblem2() + "_drawInstance"))
    eval(getProblem2() + "_drawInstance('#instance2div', (getInstance2()))");
  else
    $("#instance2div").html("");

}





//****************************** 3SAT *****************************************

function threesat_getInstance() {
  return "(p or q or (not s))\n(p or (not q) or s)";

}

function threesat_to_sat(instance) {
  return instance;
}




function addEdge(G, v1, v2) {
  for (let edge of G.edges) {
    if ((edge[0] == v1) && (edge[1] == v2))
      return;

    if ((edge[0] == v2) && (edge[1] == v1))
      return;
  }

  G.edges.push([v1, v2]);
}

function addNode(G, v) {
  G.vertices.push(v);
}



function traiterLitteral(G, v) {
  let l = v;
  if (typeof v == "object") {
    l = v[1];
  }

  let notl = "(not " + l + ")";
  addEdge(G, "R", l);
  addEdge(G, "R", notl);
  addEdge(G, l, notl);
}

function threesat_to_threecoloring(instance) {
  G = { vertices: [], edges: [] };

  addEdge(G, "R", "T");
  addEdge(G, "R", "F");
  addEdge(G, "T", "F");

  let formulaSets = instance.split("\n");

  for (let formulaString of formulaSets) {
    let formula = scheme.parser(formulaString);
    let alpha = formula[0];
    let beta = formula[2];
    if (formula.length > 3) {
      let delta = formula[4];
      traiterLitteral(G, delta);
    }
    else
      delta = "F";

    traiterLitteral(G, alpha);
    traiterLitteral(G, beta);


    alpha = scheme.prettyprint(alpha);
    beta = scheme.prettyprint(beta);
    delta = scheme.prettyprint(delta);

    let clause = formulaString;
    addEdge(G, "T", "t" + clause);
    addEdge(G, "T", "z" + clause);
    addEdge(G, "t" + clause, delta);
    addEdge(G, "z" + clause, "a" + clause);
    addEdge(G, "x" + clause, "a" + clause);
    addEdge(G, "y" + clause, "a" + clause);
    addEdge(G, "x" + clause, "y" + clause);
    addEdge(G, "x" + clause, alpha);
    addEdge(G, "y" + clause, beta);

  }




  return "(" + JSON.stringify(G) + ")";


}




function threesat_to_independentset(instance) {
  G = { vertices: [], edges: [] };

  let formulaSets = instance.split("\n");
  let ic = 0;
  let propositions = new Array();

  for (let formulaString of formulaSets) {
    let clause = scheme.parser(formulaString);

    for (let i = 0; i < clause.length; i += 2) {
      addNode(G, scheme.prettyprint(clause[i]) + ic);

      let p = "";
      if (typeof clause[i] == "object")
        p = (clause[i][1]);
      else
        p = (clause[i]);

      if (propositions.indexOf(p) < 0) {
        propositions.push(p);
      }
    }


    for (let i = 0; i < clause.length; i += 2)
      for (let j = i + 2; j < clause.length; j += 2)
        if (scheme.prettyprint(clause[i]) != scheme.prettyprint(clause[j])) {
          addEdge(G, scheme.prettyprint(clause[i]) + ic, scheme.prettyprint(clause[j]) + ic);
        }

    ic++;

  }




  for (let p of propositions) {
    let notp = "(not " + p + ")";

    for (let icc1 = 0; icc1 < ic; icc1++)
      for (let icc2 = icc1 + 1; icc2 < ic; icc2++) {

        if (G.vertices.indexOf(notp + icc1) >= 0 && G.vertices.indexOf(p + icc2) >= 0)
          addEdge(G, notp + icc1, p + icc2);

        if (G.vertices.indexOf(p + icc1) >= 0 && G.vertices.indexOf(notp + icc2) >= 0)
          addEdge(G, p + icc1, notp + icc2);
      }

  }



  G.numberofvertices = formulaSets.length; //number of clauses


  return "(" + JSON.stringify(G) + ")";


}




let threesat_to_clique = compose(independentset_to_clique, threesat_to_independentset);



//****************************** SAT *****************************************

function sat_getInstance() {
  return "(t or p or q or (not s))\n(p or (not q))";

}



function sat_to_threesat(instance) {
  let newformulaSets = new Array();
  let formulaSets = instance.split("\n");
  let compteurProposition = 2;

  for (let formulaString of formulaSets) {
    let formula = scheme.parser(formulaString);

    if (formula.length <= 5)
      newformulaSets.push(formulaString);
    else {
      let currentClause = [];
      for (let i = 0; i < formula.length; i += 2) {
        if (currentClause.length == 0)
          currentClause.push(formula[i]);
        else {
          currentClause.push("or");
          currentClause.push(formula[i]);
        }

        if (currentClause.length == 3 && (i < formula.length - 1)) {
          let newProposition = "_" + compteurProposition;
          currentClause.push("or");
          currentClause.push(newProposition);
          newformulaSets.push(scheme.prettyprint(currentClause));
          currentClause = [];
          currentClause.push(["not", newProposition]);
          compteurProposition++;
        }

      }

      newformulaSets.push(scheme.prettyprint(currentClause));
    }
  }

  return newformulaSets.join("\n");
}








//****************************** 3COL *****************************************

function threecoloring_getInstance() {
  return "({vertices: [1, 2, 3],\n" +
    "edges:  [[1, 2], [1, 3], [2, 3]]})";

}


function threecoloring_drawInstance(elementDescription, instanceCode) {
  let instance = eval("(" + instanceCode + ")");
  graphBegin();
  for (let edge of instance.edges) {
    graphAddEdge(edge[0], edge[1]);
  }
  graphEnd();

  graph_show(elementDescription);

}



let colors = ["red", "blue", "yellow"];


function createPropositionVertexInColor(v, c) {
  return "(" + v + " in " + c + ")";
}


function threecoloring_to_sat(instance) {
  let G = eval(instance);
  let phi = "";

  for (let v of G.vertices) {
    phi += "(" + createPropositionVertexInColor(v, colors[0]) + " or " + createPropositionVertexInColor(v, colors[1]) + " or " + createPropositionVertexInColor(v, colors[2]) + ")\n";
  }

  for (let v of G.vertices)
    for (let c1 of colors)
      for (let c2 of colors)
        if (c1 != c2) {
          phi += "((not " + createPropositionVertexInColor(v, c1) + ") or (not " + createPropositionVertexInColor(v, c2) + "))\n";
        }

  for (let c of colors)
    for (let e of G.edges) {
      phi += "((not " + createPropositionVertexInColor(e[0], c) + ") or (not " + createPropositionVertexInColor(e[1], c) + "))\n";


    }

  return phi;
}



function threecoloring_to_threesat(instance) {
  return sat_to_threesat(threecoloring_to_sat(instance));
}




//****************************** picross *****************************************

function picross_getInstance() {
  return "({clusterH: [[3, 6], [1, 4], [1, 1, 3], [2], [3, 3], [1, 4], [2, 5], [2, 5], [1, 1], [3]],\n" +
    "clusterV:  [[2, 3], [1, 2], [1, 1, 1, 1], [1, 2], [1, 1, 1, 1], [1, 2], [2, 3], [3, 4], [8], [9]]})";

}



/*function picross_drawInstance(elementDescription, instanceCode)
{
  let instance = eval("(" + instanceCode + ")");
  let x = instance.clusterH;
  let y = instance.clusterV;
  
  $(elementDescription).
  
}*/







function drawInstanceGraph(elementDescription, instanceCode) {
  let instance = eval("(" + instanceCode + ")");
  graphBegin();

  for (let node of instance.vertices) {
    graphAddNode(node);
  }

  for (let edge of instance.edges) {
    graphAddEdge(edge[0], edge[1]);
  }
  graphEnd();

  graph_show(elementDescription);

}




//****************************** INDEPENDENT SET *****************************************

function independentset_getInstance() {
  return "({vertices:[1,2,3,4], edges:[[1,4],[3,4]], numberofvertices: 3})";

}



function graphEdgeContains(G, u, v) {
  for (let edge of G.edges)
    if ((edge[0] == u) && (edge[1] == v))
      return true;

  for (let edge of G.edges)
    if ((edge[0] == v) && (edge[1] == u))
      return true;

  return false;
}

function graphEdgesComplement(G) {
  let Gnew = ({ vertices: G.vertices, edges: [] });

  for (let u of G.vertices)
    for (let v of G.vertices)
      if (u != v) {
        if (!graphEdgeContains(G, u, v)) {
          if (!graphEdgeContains(Gnew, u, v))
            Gnew.edges.push([u, v]);

        }

      }

  return Gnew;

}

function independentset_to_clique(instanceCode) {
  let instance = eval("(" + instanceCode + ")");
  instanceN = graphEdgesComplement(instance);
  instanceN.numberofvertices = instance.numbervertices;
  return "(" + JSON.stringify(instanceN) + ")";
}


let independentset_drawInstance = drawInstanceGraph;







//****************************** INDEPENDENT SET *****************************************

function clique_getInstance() {
  return "({vertices: [1, 2, 3, 4],  edges:  [[1, 2], [1, 3], [2, 3], [2, 4]], numberofvertices: 3})";

}

let clique_to_independentset = independentset_to_clique;



let clique_drawInstance = drawInstanceGraph;

