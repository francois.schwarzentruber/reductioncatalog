function xitj(i, t, j) {
	return "(xgauche du cluster numero " + t + " ligne " + i + " vaut " + j + ")";
}



function yjti(j, t, i) {
	return "(yhaut du cluster numero " + t + " colonne " + j + " vaut " + i + ")";
}

function peint(i, j) {
	return "(case " + i + " " + j + " noire)";
}


/**
 * @example disjonctionCluster(3, [1, 4], 5, function(x, t) {return "(cluster " + t + " à pos + " + x + ")";})
 */
function disjonctionCluster(x, clusters, n, functionForMakingAtoms, minBounds, maxBounds) {
	let atoms = new Array();

	for (let t in clusters) {
		let posmin = Math.max(minBounds(t), x - clusters[t] + 1);
		let posmax = Math.min(x, maxBounds(t));


		for (let xx = posmin; xx <= posmax; xx++) {
			atoms.push(functionForMakingAtoms(xx, t));

		}

	}

	if (atoms.length == 0)
		return "bottom";
	else if (atoms.length == 1) {
		return atoms[0];
	}
	else {
		let s = "(";

		for (let i in atoms) {
			s += atoms[i];

			if (i < atoms.length - 1) {

				s += " or ";
			}
		}

		s += ")";

		return s;

	}


}





function createBigOrDisjunction(letiable, boundmin, boundmax, subformula) {

	if (boundmin == boundmax) {
		let regex = new RegExp(" " + letiable + "\\)", 'g');
		subformula = subformula.replace(regex, " " + boundmin + ")");
		return subformula;
	}
	else {
		return "(bigor " + letiable + " (" + boundmin + " .. " + boundmax + ") " + subformula + ")";

	}
}



function picrossToSAT(clusterH, clusterV) {
	let formulas = new Array();
	let nbcolonnes = clusterV.length;
	let nblignes = clusterH.length;

	let getPosMinClusterH = function (i, t) {
		let s = 0;
		for (let t2 = 0; t2 < t; t2++) {
			s += clusterH[i][t2] + 1;
		}

		return s;

	}

	let getPosMinClusterV = function (i, t) {
		let s = 0;
		for (let t2 = 0; t2 < t; t2++) {
			s += clusterV[i][t2] + 1;
		}

		return s;

	}

	let getPosMaxClusterH = function (i, t) {
		let s = nbcolonnes;
		for (let t2 = clusterH[i].length - 1; t2 > t; t2--) {
			s -= (clusterH[i][t2] + 1);
		}

		s -= clusterH[i][t];
		return s;

	}

	let getPosMaxClusterV = function (i, t) {
		let s = nbcolonnes;
		for (let t2 = clusterV[i].length - 1; t2 > t; t2--) {
			s -= (clusterV[i][t2] + 1);
		}

		s -= clusterV[i][t];
		return s;

	}



	/*each horizontal cluster has a position j*/
	for (let i in clusterH) {
		for (let t in clusterH[i]) {
			formulas.push(createBigOrDisjunction("j", getPosMinClusterH(i, t), getPosMaxClusterH(i, t), xitj(i, t, "j")));
		}
	}

	/*.. and this position is unique*/
	for (let i in clusterH) {
		for (let t in clusterH[i])
			for (j = getPosMinClusterH(i, t); j <= getPosMaxClusterH(i, t); j++)
				for (j2 = getPosMinClusterH(i, t); j2 <= getPosMaxClusterH(i, t); j2++)
					if (j != j2) {
						formulas.push("(" + xitj(i, t, j) + " imply (not " + xitj(i, t, j2) + "))");
					}
	}



	/*each vertical cluster has a position i*/
	for (let j in clusterV) {
		for (let t in clusterV[j]) {
			formulas.push(createBigOrDisjunction("i", getPosMinClusterV(j, t), getPosMaxClusterV(j, t), yjti(j, t, "i")));
		}
	}

	/*.. and this position is unique*/
	for (let j in clusterV) {
		for (let t in clusterV[j])
			for (let i = getPosMinClusterV(j, t); i <= getPosMaxClusterV(j, t); i++)
				for (i2 = getPosMinClusterV(j, t); i2 <= getPosMaxClusterV(j, t); i2++)
					if (i != i2) {
						formulas.push("(" + yjti(j, t, i) + " imply (not " + yjti(j, t, i2) + "))");
					}
	}


	/*the next horizontal cluster is on the right*/
	for (let i in clusterH) {
		for (let t in clusterH[i])
			if (t < clusterH[i].length - 1) {
				for (let j = getPosMinClusterH(i, t); j <= getPosMaxClusterH(i, t); j++) {

					let t = parseInt(t);
					let j = parseInt(j);
					let posmin = j + clusterH[i][t] + 1;
					let posmax = getPosMaxClusterH(i, t + 1);

					formulas.push("(" + xitj(i, t, j) + " imply (bigor j2 (" + posmin + " .. " + posmax + ") " + xitj(i, t + 1, "j2") + "))");

				}

			}

	}

	/*the next vertical cluster is on the bottom*/
	for (let j in clusterV) {
		for (let t in clusterV[j])
			if (t < clusterV[j].length - 1) {
				for (let i = getPosMinClusterV(j, t); i <= getPosMaxClusterV(j, t); i++) {

					let t = parseInt(t);
					let i = parseInt(i);
					let posmin = i + clusterV[j][t] + 1;
					let posmax = getPosMaxClusterV(j, t + 1);

					formulas.push("(" + yjti(j, t, i) + " imply (bigor i2 (" + posmin + " .. " + posmax + ") " + yjti(j, t + 1, "i2") + "))");

				}

			}

	}




	/*a horizontal cluster makes black cells*/
	for (let i in clusterH) {
		for (let t in clusterH[i]) {
			for (let j = getPosMinClusterH(i, t); j <= getPosMaxClusterH(i, t); j++) {

				let j = parseInt(j);
				let t = parseInt(t);
				let posmax = (j + clusterH[i][t] - 1);

				formulas.push("(" + xitj(i, t, j) + " imply (bigand j2 (" + j + " .. " + posmax + ") " + peint(i, "j2") + "))");

			}

		}
	}


	/*a vertical cluster makes black cells*/
	for (let j in clusterV) {
		for (let t in clusterV[j]) {
			for (let i = getPosMinClusterV(j, t); i <= getPosMaxClusterV(j, t); i++) {

				let i = parseInt(i);
				let t = parseInt(t);
				let posmax = (i + clusterV[j][t] - 1);

				formulas.push("(" + yjti(j, t, i) + " imply (bigand i2 (" + i + " .. " + posmax + ") " + peint("i2", j) + "))");

			}

		}
	}



	/*a black cell belongs to a horizontal cluster and a vertical cluster*/
	for (let i in clusterV) {
		for (let j in clusterH) {
			formulas.push("(" + peint(i, j) + " imply " + disjonctionCluster(j, clusterH[i], nbcolonnes,
				function (j2, t) { return xitj(i, t, j2); },
				function (t) { return getPosMinClusterH(i, t); },
				function (t) { return getPosMaxClusterH(i, t); }
			) + ")");
			formulas.push("(" + peint(i, j) + " imply " + disjonctionCluster(i, clusterV[j], nbcolonnes,
				function (i2, t) { return yjti(j, t, i2); },
				function (t) { return getPosMinClusterV(j, t); },
				function (t) { return getPosMaxClusterV(j, t); }
			) + ")");


		}



	}










	// 	let formula = "((not bottom) and ";
	// 	
	// 	
	// 	
	// 	for(let i in formulas)
	// 	{
	// 	    formula += formulas[i];
	// 	    if(i < formulas.length - 1)
	// 	    {
	// 		formula += " and \n";
	// 	    }
	// 	  
	// 	}
	// 
	// 	
	// 	formula += ")";
	// 	return formula;


	let formula = "(not bottom)\n";

	for (let i in formulas) {
		formula += formulas[i];
		if (i < formulas.length - 1) {
			formula += " \n";
		}

	}
	return formula;

}




function picross_to_sat(instance) {
	let obj = eval(instance);

	let clusterH = obj.clusterH;
	let clusterV = obj.clusterV;

	return picrossToSAT(clusterH, clusterV);

}